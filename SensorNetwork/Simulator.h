#pragma once
#include "TemperatureSensor.h"

#include <vector>

class Simulator
{
public:
	Simulator();
	~Simulator();

	void simulate();

private:
	std::vector<TemperatureSensor*> _tempSensors;
};