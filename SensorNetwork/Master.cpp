#include "Master.h"

#include <iostream>

std::shared_ptr<Master> Master::_myself = nullptr;

std::shared_ptr<Master> Master::getMaster()
{
	if (_myself != nullptr)
	{
		return _myself;
	}
	else
	{
		_myself = std::make_shared<Master>(Master());
		return _myself;
	}

}

void Master::update(Message m)
{
	std::cout << m.get() << std::endl;
}
