#pragma once
#include "IObserver.h"
#include "Message.h"

#include <memory>

class Master : public IObserver
{
private:
	Master() = default;
public:
	static std::shared_ptr<Master> getMaster();
	void update(Message) override;
private:
	static std::shared_ptr<Master> _myself;
};