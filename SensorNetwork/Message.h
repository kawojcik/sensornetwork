#pragma once

#include <string>

class Message
{
public:
	Message(std::string message) : _message(message)
	{
	}

	virtual std::string get()
	{
		return _message;
	}
protected:
	std::string _message;
};