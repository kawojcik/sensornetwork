#include "Simulator.h"
#include "Master.h"

#include <thread>
#include <random>

static const int SENSORS_NUM = 10;

Simulator::Simulator()
{
	std::shared_ptr<Master> master = Master::getMaster();

	for (int i = 0; i < SENSORS_NUM; ++i)
	{
		_tempSensors.push_back(new TemperatureSensor(100 + i));
	}

	// generate random temperatures
	std::random_device d;
	std::mt19937 mt(d());
	std::uniform_int_distribution<int> dist(-40, 40);
	for (auto sensor : _tempSensors)
	{
		sensor->setTemperature(dist(mt));
		sensor->attach(master);
	}
}

Simulator::~Simulator()
{
	for (auto tempSensor : _tempSensors)
	{
		delete tempSensor;
	}
}

void Simulator::simulate()
{
	std::vector<std::thread> threads;

	std::thread t0([&]() { _tempSensors[0]->notify(); });
	std::thread t1([&]() { _tempSensors[1]->notify(); });
	std::thread t2([&]() { _tempSensors[2]->notify(); });
	std::thread t3([&]() { _tempSensors[3]->notify(); });
	std::thread t4([&]() { _tempSensors[4]->notify(); });
	std::thread t5([&]() { _tempSensors[5]->notify(); });
	std::thread t6([&]() { _tempSensors[6]->notify(); });
	std::thread t7([&]() { _tempSensors[7]->notify(); });
	std::thread t8([&]() { _tempSensors[8]->notify(); });
	std::thread t9([&]() { _tempSensors[9]->notify(); });

	threads.push_back(std::move(t0));
	threads.push_back(std::move(t1));
	threads.push_back(std::move(t2));
	threads.push_back(std::move(t3));
	threads.push_back(std::move(t4));
	threads.push_back(std::move(t5));
	threads.push_back(std::move(t6));
	threads.push_back(std::move(t7));
	threads.push_back(std::move(t8));
	threads.push_back(std::move(t9));

	std::for_each(threads.begin(), threads.end(), [](std::thread& t) { t.join(); });
}