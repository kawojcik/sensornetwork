#pragma once

class Message;

class IObserver
{
public:
	IObserver() = default;

	virtual void update(Message) = 0;
};
