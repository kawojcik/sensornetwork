#include "TemperatureSensor.h"
#include "IObserver.h"

#include <sstream>

TemperatureSensor::TemperatureSensor(int id) : _id(id)
{
}

void TemperatureSensor::attach(std::shared_ptr<IObserver> observer)
{
	_observer = observer;
}

void TemperatureSensor::detach()
{
	_observer = nullptr;
}

void TemperatureSensor::notify()
{
	std::stringstream m;
	m << "Messege from temperature sensor " << _id << ": " << _degrees_celsius << " C degrees";
	if (_observer != nullptr)
	{
		_observer->update(Message(m.str()));
	}
}

void TemperatureSensor::setTemperature(int temp)
{
	_degrees_celsius = temp;
}

int TemperatureSensor::getTeperature() const
{
	return _degrees_celsius;
}
