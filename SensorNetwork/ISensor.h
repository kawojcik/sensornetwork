#pragma once

#include <memory>

class IObserver;

class ISensor
{
public:
	ISensor() = default;
	
	virtual void attach(std::shared_ptr<IObserver>) = 0;
	virtual void detach() = 0;
	virtual void notify() = 0;
protected:
	std::shared_ptr<IObserver> _observer;
};