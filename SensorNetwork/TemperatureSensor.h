#pragma once
#include "ISensor.h"
#include "Message.h"

class TemperatureSensor : public ISensor
{
public:
	TemperatureSensor(int id);

	void attach(std::shared_ptr<IObserver>) override;
	void detach() override;
	void notify() override;

	void setTemperature(int temp);
	int getTeperature() const;
private:
	int _id;
	int _degrees_celsius;
};