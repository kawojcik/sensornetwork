# SensorNetwork

### How to build project

1. `git clone https://gitlab.com/kawojcik/sensornetwork.git`
2. `cd sensornetwork`
3. `mkdir build`
4. `cd build`
5. `cmake ..`
6. open .sln file in Visual Studio
7. Build (eg. Ctrl + B)
8. Run (open console in build\SensorNetwork\Debug folder and run command `SensorNetwork.exe`)